# Simple Contact Management DataBase Application

## Instructions

Write Simple Contact Management DataBase Application (Console)

---

## Functionality:

Add/find(display) - list all/remove contact
add/remove phone number TO contact
Save data to a file
Load data from a file

Example data structure (simplified version)
"names.db"

     4 bytes id
     1 byte name len
     0 - 255 bytes name
     1 byte of number of phone numbers
     1 byte desc len
     0 - 255 bytes phone desc
     1 byte number len
     0 - 255 bytes phone number

---

Feel free to experiment with a 'relational' method to your data management!

---

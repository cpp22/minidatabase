#ifndef __DATABASEMANAGER_H__
#define __DATABASEMANAGER_H__

class DataBaseManager
{

public:
    //Appends db with new contact
    static void WriteContactData(const Contact &contact);

    //Creates the Contact Database with header
    static void InitDataBase();

    //Reads Contact db and return hude data blob pointer and it's size
    static uint8_t *ReadDataBase(uint32_t *size);

    //write data from string to database, does NOT enforce correct size
    static void WriteStringData(const std::string &data, const int &fileHandle);

    //write data from string to database and receives size checker to increment, does NOT enforce correct size
    static void WriteStringData(const std::string &data, const int &fileHandle, size_t *totalSize);

    //converts data blob to contact* vector, remember to free memory
    static std::vector<Contact *> *ParseContact(uint8_t *data, const uint32_t &size);

    //COUT data in big data blob
    static void CoutData(uint8_t *data, const uint32_t &size);
};
#endif // __DATABASEMANAGER_H__
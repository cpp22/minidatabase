#ifndef __CONTACT_H__
#define __CONTACT_H__

#include <stdint.h>

struct Contact
{
    static uint32_t Counter; //TODO:not in use yet
    uint32_t id;             //contact ID on database
    std::string name;        //contact's name, 256 max characters size
    std::string description; //contact's description, 256 max characters size
};
#endif // __CONTACT_H__
#ifndef __CONSTANTS_H__
#define __CONSTANTS_H__

#include <stdint.h>

const static size_t STRING_SIZE = 1;                               //string max size in bytes
const static size_t LIST_MAX_SIZE = 4;                             //pre defined max database capacity, 32 bits as key
const static char FILE_NAME[] = "contacts.db";                     //contacts database defalt name
const static size_t MAX_DATA_SIZE = 7 + (256 * 3);                 //max amount of bytes to be written in a file at once
const static int SIGNATURE = 'CEAP';                               //predefined signature of database header
const static int8_t VERSION = 1;                                   //current version
const static char COPYRIGHT[] = "Copyright of Pedro Soares, 2021"; //my watermark in Database header

//current header default size considering the removal of null char
const static size_t FILE_HEADER = sizeof(COPYRIGHT) - 1 + sizeof(SIGNATURE) + sizeof(VERSION);

#endif // __CONSTANTS_H__
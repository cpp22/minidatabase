CXX		  := g++
CXX_FLAGS := -Wall -Wextra -std=c++17 -ggdb


BIN		:= bin
SRC		:= src
INCLUDE	:= include
LIB		:= lib

LIBRARIES	:=
EXECUTABLE	:= main


all: $(BIN)/$(EXECUTABLE)

run: clean all
	clear
	./$(BIN)/$(EXECUTABLE)

$(BIN)/$(EXECUTABLE): $(SRC)/*.cpp
	$(CXX) $(CXX_FLAGS) -I$(INCLUDE) -L$(LIB) $^ -o $@ $(LIBRARIES)

clean:
	-rm $(BIN)/*

memoryTest: $(BIN)/$(EXECUTABLE)
	valgrind $(BIN)/$(EXECUTABLE)

# $(BIN)/disassembly.S:
# 	$(CXX) $(CXX_FLAGS) -I$(INCLUDE) -L$(LIB) $^ -g -o $(BIN)/disassembly.S $(SRC)/*.cpp $(LIBRARIES)

# objdump:
# 	objdump -d $(BIN)/compiled.bin -l > $(BIN)/disassembly.S
#include <iostream>
#include <vector>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include "constants.h"
#include "contact.h"
#include "dataBaseManager.h"

int main()
{
    std::cout << "Hello Simple Contact Management DataBase" << std::endl;

    Contact test;
    test.id = 0;
    test.name = "Cesar A";
    test.description = "GT01 colleague";

    Contact test2;
    test2.id = 1;
    test2.name = "Gavin";
    test2.description = "GT01 DUDE";

    Contact test3;
    test3.id = 2;
    test3.name = "Arnav";
    test3.description = "GT01 Guy";

    DataBaseManager::InitDataBase();

    // DataBaseManager::WriteContactData(test);
    // DataBaseManager::WriteContactData(test2);
    // DataBaseManager::WriteContactData(test3);

    unsigned int dataSize = 0;
    auto data = DataBaseManager::ReadDataBase(&dataSize);

    DataBaseManager::CoutData(data, dataSize);

    auto contacts = DataBaseManager::ParseContact(data, dataSize);

    for (auto parseTest : *contacts)
    {
        std::cout << "parse test id: " << parseTest->id << std::endl;
        std::cout << "parse test name: " << parseTest->name << std::endl;
        std::cout << "parse test desc: " << parseTest->description << std::endl;
    }

DELETE:
    for (auto contact : *contacts)
    {
        delete contact;
    }
    contacts->clear();
    delete contacts;
    delete[] data; // TODO: verify if it indeed frees ALL the data

    std::cout
        << "ERRNO: " << errno << std::endl;
    return 0;
}
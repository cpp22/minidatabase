#include <vector>
#include <iostream> // FIXME: remove later
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include "constants.h"
#include "contact.h"
#include "dataBaseManager.h"

void DataBaseManager::WriteContactData(const Contact &contact)
{
    {
        struct flock fLock;          //not locking anymore
        fLock.l_type = F_WRLCK;      //can be read from;
        fLock.l_whence = SEEK_END;   // as append
        fLock.l_start = 0;           //no offset
        fLock.l_len = MAX_DATA_SIZE; //max write size(normally less)

        int modeO = O_WRONLY | O_APPEND;                                //only write and appends
        int file = open(FILE_NAME, modeO, S_IRWXG | S_IRWXO | S_IRWXU); //FIXME: change S_Flags
        if (file == -1)
        {
            InitDataBase();
            //WriteContactData(contact); //TODO: test for infinity loop
            return;
        }
        lseek(file, 0, SEEK_END); //just too be sure of append

        std::cout << "lock: " << fcntl(file, F_SETLK, &fLock) << std::endl; // locking the file

        size_t dataSize = LIST_MAX_SIZE; // to increment

        write(file, &(contact.id), LIST_MAX_SIZE);
        WriteStringData(contact.name, file, &dataSize);
        WriteStringData(contact.description, file, &dataSize);

        std::cout << "Total Data Size: " << dataSize << std::endl; //total size

        close(file); //unlock automatically on close
    }
}

void DataBaseManager::InitDataBase()
{

    int modeO = O_WRONLY | O_CREAT;

    int file = open(FILE_NAME, modeO, S_IRUSR | S_IWUSR);

    write(file, &SIGNATURE, sizeof(SIGNATURE));
    write(file, &VERSION, sizeof(VERSION));
    write(file, &COPYRIGHT, sizeof(COPYRIGHT) - 1); //remove null value terminated char
}

uint8_t *DataBaseManager::ReadDataBase(uint32_t *size)
{
    int modeO = O_RDONLY;
    int file = open(FILE_NAME, modeO, S_IRUSR | S_IWUSR);

    if (file == -1)
    {
        InitDataBase();
        *size = 0;
        return nullptr;
    }
    int sig;
    char ver;
    char cr[32];
    ssize_t r = read(file, &sig, sizeof(SIGNATURE));
    if (sig != SIGNATURE)
    {
        std::cout << "Sig ERROR" << std::endl;
    }
    else
    {
        std::cout << " - SIG: " << sig << std::endl;
    }

    r = read(file, &ver, sizeof(VERSION));

    if (ver != VERSION)
    {
        std::cout << " - "
                  << "Ver ERROR"
                  << " - ";
    }
    else
    {
        std::cout << " - Ver:" << (int)ver << " - ";
    }

    r = read(file, &cr, (sizeof(COPYRIGHT) - 1)); //-1 due null char at end

    std::cout << cr << std::endl;

    *size = lseek(file, -FILE_HEADER, SEEK_END);
    lseek(file, FILE_HEADER, SEEK_SET); // goes to first ID
    std::cout << "DATA size: " << *size << std::endl;
    uint8_t *data = new uint8_t[*size];
    std::cout << "Number of Bytes read: " << read(file, data, *size) << std::endl;

    return data;
}

void DataBaseManager::WriteStringData(const std::string &data, const int &fileHandle)
{
    uint8_t stringLength = data.length();
    write(fileHandle, &stringLength, STRING_SIZE);
    write(fileHandle, data.c_str(), STRING_SIZE * stringLength);
}

void DataBaseManager::WriteStringData(const std::string &data, const int &fileHandle, size_t *totalSize)
{
    uint8_t stringLength = data.length();
    *totalSize += stringLength * STRING_SIZE;
    write(fileHandle, &stringLength, STRING_SIZE);
    write(fileHandle, data.c_str(), STRING_SIZE * stringLength);
}

std::vector<Contact *> *DataBaseManager::ParseContact(uint8_t *data, const uint32_t &size)
{
    if (size < 0)
        return nullptr;

    auto contactVec = new std::vector<Contact *>();
    unsigned int counter = 0;

    while (counter < size)
    {
        Contact *contact = new Contact;
        //ID data
        uint32_t id = 0;
        for (int i = LIST_MAX_SIZE - 1; i >= 0; --i)
        {
            id << LIST_MAX_SIZE;
            id |= data[i + counter];
        }
        counter += 4;

        uint8_t nameLen = 0;
        nameLen = data[counter];
        counter += 1;
        //Name data
        char name[nameLen + 1];
        for (int i = 0; i < nameLen; ++i)
        {
            name[i] = data[i + counter];
        }
        name[nameLen] = '\0'; //null termination on char array;
        counter += nameLen;

        uint8_t descLen = 0;
        descLen = data[counter];
        counter += 1;
        //Name data
        char description[descLen + 1];
        for (int i = 0; i < descLen; ++i)
        {
            description[i] = data[i + counter];
        }
        description[descLen] = '\0'; //null termination on char array;
        counter += descLen;

        contact->id = id;
        contact->name = name;
        contact->description = description;

        contactVec->emplace_back(contact);
    }

    return contactVec;
}

void DataBaseManager::CoutData(uint8_t *data, const uint32_t &size)
{
    std::cout << "DATA size GOT: " << size << std::endl;
    std::cout << "____DATA________________________________";
    for (unsigned int i = 0; i < size; ++i)
    {
        if (i % 12 == 0)
        {
            std::cout << std::endl
                      << "| ";
        }

        if (data[i] > 31 && data[i] < 122)
            std::cout << (char)data[i] << " ";
        else
            std::cout << (int)data[i] << " ";
    }
    std::cout << std::endl
              << "--END-DATA-------------------------" << std::endl;
}
